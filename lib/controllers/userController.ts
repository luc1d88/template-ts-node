import * as mongoose from "mongoose";
import { User } from "../models/domain/User";
import { userSchema, IUserModel } from "../models/persist/userSchema";
import { Request, Response } from "express";
import { IUserFullDTO, IUserSimpleDTO } from "../models/dto/UserDTOs";

const UserModel = mongoose.model<IUserModel>('User', userSchema);

export class UserController {

  public addNewUser(req: Request, res: Response) {
    let user: User = new User(
      req.body.firstName,
      req.body.lastName,
      req.body.email);
    let newUser = new UserModel(user);

    newUser.save((err: mongoose.Document, _user: IUserModel) => {
      if (err){
        throw new Error("BROKEN"); 
      }
      user.email = _user._id;
      const userDTO: IUserFullDTO = user.mapToFullDTO();
      return res.json(userDTO);
    });
  }

  public listUsers(req: Request, res: Response) {
    const query = UserModel.find({});
    query.sort({lastName: 1});
    query.lean()
    query.exec((err: mongoose.Document, users: any) => {
      if (err) {
        throw new Error("BROKEN");
      }
      else if (!users) {
        return res.json({'message': 'Users not found!'})
      }
      let usersDTO: IUserSimpleDTO[] = [];
      usersDTO = users.map((_user: IUserModel) => {
        const user: User = new User(_user.email, _user.firstName, _user.lastName, _user._id);
        const userDTO: IUserSimpleDTO = user.mapToSimpleDTO();
        return userDTO;
      })
      return res.json(usersDTO);
    });
  }
}
