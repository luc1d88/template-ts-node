import { Document } from "mongoose";
import { IUserSimpleDTO, IUserFullDTO } from "../dto/UserDTOs";

export class User {
  private _uid: string;
  private _email: string;
  private _firstName: string;
  private _lastName: string;

  constructor(email: string, firstName: string, lastName: string, uid?: string) {
    this._email = email;
    this._firstName = email;
    this._lastName = email;
    this._uid = uid ? uid : null;
  }

  // Getters and setters
  get uid(): string {
    return this._uid;
  }
  set uid(uid: string) {
    this._uid = uid;
  }
  get firstName(): string {
    return this._firstName;
  }
  set firstName(firstName: string) {
    this._firstName = firstName;
  }
  get lastName(): string {
    return this._lastName;
  }
  set lastName(lastName: string) {
    this._lastName = lastName;
  }
  get email(): string {
    return this._email;
  }
  set email(email: string) {
    this._email = email;
  }

  // methods
  mapToSimpleDTO(): IUserSimpleDTO {
    const dto = ({
      uid: this._uid,
      firstName: this._firstName,
      lastName: this._lastName,
    });
    return dto ;  }
  mapToFullDTO(): IUserFullDTO {
    const dto = ({
      uid: this._uid,
      firstName: this._firstName,
      lastName: this._lastName,
      email: this._email
    });
    return dto ;
  }
}
