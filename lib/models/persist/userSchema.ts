import { Schema } from "mongoose";
import * as mongoose from "mongoose";
import { User } from "../domain/User";

export var userSchema: Schema = new Schema({
  email: String,
  firstName: String,
  lastName: String
});

export interface IUserModel extends User, mongoose.Document {}
