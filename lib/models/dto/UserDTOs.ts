export interface IUserSimpleDTO {
  uid: string;
  firstName: string;
  lastName: string;
}

export interface IUserFullDTO {
  uid: string;
  email: string;
  firstName: string;
  lastName: string;
}
