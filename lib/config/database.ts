import mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

export class DatabaseConfig {
  private MONGODB_CONNECTION: string;

  constructor() {
    this.MONGODB_CONNECTION= "mongodb://localhost:27017/typescript";
  }

  configureDatabase() {
    const connect: Promise<typeof mongoose>  = mongoose.connect(this.MONGODB_CONNECTION);
    const connection = mongoose.connection;
    connection.once('error', (err) => {
      console.log('Error on opening db connection: ' + err);
    });  connection.once('open', () => {
      console.log('Database connection opened');
    });
  }
}
