import * as passport from 'passport';
import { BasicStrategy } from 'passport-http';

export class AuthenticationConfig {
  private username: string;
  private password: string;
  private passport: passport.PassportStatic;

  constructor() {
    this.passport = passport;
    this.username = 'admin';
    this.password = 'admin';
  }

  configureBasicAuthentication() {
    this.passport.use(new BasicStrategy((userid, password, done)  => {
      if(userid === this.username && password === this.password)
        return done(null, this.username);
      else {
        return done(false, this.username);
      }
    }));
  }
}
