import * as express from "express";
import * as bodyParser from "body-parser";
import * as morgan from 'morgan';
import { RootRouter } from '../routes/rootRoutes';
import { DatabaseConfig } from "../config/database";
import { AuthenticationConfig } from '../config/authentication';

class App {
  public app: express.Application;
  public rootRouter: RootRouter;
  public database: DatabaseConfig;
  public authentication: AuthenticationConfig;

  constructor() {
    this.app = express();
    this.database = new DatabaseConfig();
    this.authentication = new AuthenticationConfig();
    this.rootRouter = new RootRouter();
    this.configure();
  }

  private configure(): void {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(morgan('dev'));
    this.app.use(this.errorHandler);
    this.app.use('/', this.rootRouter.router);
    this.database.configureDatabase();
    this.authentication.configureBasicAuthentication();
  }

  private errorHandler(err, req, res, next) {
    res.status(500).send(err);
    next();
  }
}

export default new App().app;
