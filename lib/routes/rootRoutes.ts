import * as express from "express";
import { Request, Response } from "express";
import { AdminRouter } from './adminRoutes';
import { UserRouter } from './userRoutes';
import * as passport from 'passport';

export class RootRouter{
  public router: express.Router;
  private userRouter: UserRouter;
  private adminRouter: AdminRouter;
  
  constructor() {
    this.router = express.Router();
    this.userRouter = new UserRouter();
    this.adminRouter = new AdminRouter();
    this.configureRoutes();
  }
  
  configureRoutes() {
    this.router.use('/users', this.userRouter.router);
    this.router.use('/admin', passport.authenticate('basic', { session: false }), this.adminRouter.router);
  }
}
