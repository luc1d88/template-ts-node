import * as express from "express";
import { Request, Response } from "express";
import { AdminController } from "../controllers/adminController";

export class AdminRouter {
  private adminController: AdminController;
  public router: express.Router;

  constructor() {
    this.router = express.Router();
    this.adminController = new AdminController();
    this.configureRoutes();
  }
  
  configureRoutes() {
    this.router.get('/', this.adminController.hello);
  }
}
