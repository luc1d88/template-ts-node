import * as express from "express";

import { Request, Response } from "express";
import { UserController } from "../controllers/userController";

export class UserRouter {
  private userController: UserController;
  public router: express.Router;

  constructor() {
    this.router = express.Router();
    this.userController = new UserController();
    this.configureRoutes();
  }
  
  configureRoutes() {
    this.router.get('/', this.userController.listUsers);
    this.router.post('/', this.userController.addNewUser);
  }
}
