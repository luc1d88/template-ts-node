import app from "./app/app";
const port = 4040;
app.listen(port, function() {
  console.log('Express server listening on port ' + port);
});